from flask import request
from flask_cors import CORS
import conf.read_config as conf_reader
from alphamissense.setup import setup
from alphamissense.get_score import get_score, get_scores_for_region
from alphamissense.tools import parse_genome_position

DEBUG = True

from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)
app.config.from_object(__name__)

SERVICE_NAME='alphamissense'
VERSION='v1'

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = '/alphamissense/v1/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "AlphaMissense Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/getScore', methods=['GET'])
def get_interpretations(genome_version=None):
    genomepos = request.args.get("q")
    if genomepos is not None:
        genome_version = genome_version
        if genome_version == "hg38":
            res = get_score(genomepos)
            return res
        else:
            print("genome version error: ",genome_version)
            return {}
    else:
        return {}

@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/getRegionScores', methods=['GET'])
def get_region_scores(genome_version=None):
    chrom = request.args.get("chrom")
    pos_start = request.args.get("pos_start")
    pos_end = request.args.get("pos_end")
    #ref = request.args.get("ref")
    #alt = request.args.get("alt")
    if chrom is not None:
        genome_version = genome_version
        if genome_version == "hg38":
            res = get_scores_for_region(chrom,pos_start,pos_end)
            return res
        else:
            print("genome version error: ",genome_version)
            return {}
    else:
        return {}

if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    setup()
    app.run(host='0.0.0.0', debug=True, port=conf_reader.config["DEFAULT"]["FLASK_PORT"])
