import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "MNT_PATH" in os.environ:
    __MNT_PATH__ = os.getenv("MNT_PATH")
else:
    __MNT_PATH__ = config['DEFAULT']['MNT_PATH']

