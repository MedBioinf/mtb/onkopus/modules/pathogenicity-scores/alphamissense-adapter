# AlphaMissense Adapter

Module for providing AlphaMissense scores [1] for predicting the pathogenicity of missense variants. 

Get AlphaMissense predictions for a single nucleotide variant by accessing the API: (default port: 10162)

```
http://localhost:10162/alphamissense/v1/hg38/getScore?q=chr7:140753336A>T
```

Access the public API at 
```
https://mtb.bioinf.med.uni-goettingen.de/alphamissense/v1/hg38/getScore?q=chr7:140753336A>T
```

#### References

[1] Cheng, J., Novati, G., Pan, J., Bycroft, C., Žemgulytė, A., Applebaum, T., ... & Avsec, Ž. (2023). Accurate proteome-wide missense variant effect prediction with AlphaMissense. Science, eadg7492.

