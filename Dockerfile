FROM python:3.8.18-bullseye

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y tabix

#RUN python3 -m pip install --upgrade pip
RUN python3 -m venv /opt/venv
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN source /opt/venv/bin/activate
RUN python3 -m pip install --upgrade pip

WORKDIR /app/alphamissense-adapter
COPY ./requirements.txt /app/alphamissense-adapter/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/alphamissense-adapter/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10162

CMD [ "/opt/venv/bin/python3", "/app/alphamissense-adapter/app.py" ]
