import os, subprocess
import conf.read_config as conf_reader


def setup():
    """
    Check if database files exists, download them if they cannot be found

    :return:
    """
    hg38_file = conf_reader.__MNT_PATH__ + "/" + "AlphaMissense_hg38.tsv.gz"
    if not os.path.isfile(hg38_file):
        print("Downloading AlphaMissense database file (hg38)...")
        os.system("wget -v https://zenodo.org/record/8208688/files/AlphaMissense_hg38.tsv.gz?download=1 -P "+conf_reader.__MNT_PATH__ + "/ ")
        os.system("mv -v " + conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz?download=1 "+ conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")
        os.system("gunzip "+ conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")
        os.system("sed -i '/^#/d' " + conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv")
        os.system("bgzip -c " + conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv > " + conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")
        #sed -i '1 i chr\thg19_pos\tgrch38_pos\tref\talt\taaref\taaalt\tREVEL\tEnsembl_transcriptid' revel_with_transcript_ids_hg38_sorted.tsv
        os.system("tabix -S1 -s1 -b2 -e2 "+ conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")
        print("Database file downloaded")
