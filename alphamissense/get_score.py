import os, subprocess, re
import traceback

import conf.read_config as conf_reader
from alphamissense.tools import parse_genome_position


def parse_variant_exchange(variant_exchange):
    rsregex = "([A-Z]+)([0-9]+)([A-Z\\?\\*=.]+)"
    if re.compile(rsregex).match(variant_exchange):
        p = re.compile(rsregex).match(variant_exchange).groups()
        aaref = p[0]
        pos = p[1]
        aaalt = p[2]
        return aaref, pos, aaalt
    else:
        return None, None, None

def generate_result_region(gene, tabix_res,request_result):
    lines = tabix_res.split("\n")
    request_result[gene] = {}
    request_result[gene]["positions"] = []

    pos_list = {}
    for line in lines:
        try:
            #print("line ", line)
            elements = line.split("\t")
            #aaexch = elements[0] + ":" + elements[1] + elements[2] + ">" + elements[3]
            genome_pos = elements[1]
            associated_gene = elements[5]
            aa_exchange = elements[7]

            #print(associated_gene)
            #if associated_gene != gene:
            #    print("NO MATCH ",associated_gene)
            #    #pass
            #else:
            #    print(associated_gene)

            aaref, pos, aaalt = parse_variant_exchange(aa_exchange)
            #print(aaref + ":" +pos + ":" + aaalt)
            if pos not in pos_list:
                pos_list[pos] = {}
                pos_list[pos]["score"] = float(elements[8])
                pos_list[pos]["count"] = 1
            else:
                pre_score = pos_list[pos]["score"]
                count = pos_list[pos]["count"]
                new_score = (count * pre_score + float(elements[8]))/(count+1)
                pos_list[pos]["count"] = count + 1
                pos_list[pos]["score"] = float(new_score)
        except:
            print(traceback.format_exc())

    for pos in pos_list.keys():
        #print(pos + ":" + gene + ":")
        request_result[gene][pos] = pos_list[pos]["score"]
        request_result[gene]["positions"].append(pos)
    #if (elements[2] == aaref) and (elements[3] == aaalt):

    return request_result

def round_score(score,digits=3):
    return round(float(score), digits)

def generate_result(chrom,pos,ref,alt,positions,chrom_pos_positions,tabix_res):
    res = {}

    for position in positions:
        res[position] = {}

    # Test if tabix result for exactly that variant has been found
    #chr, ref_seq, pos, ref, alt = parse_genome_position(position)
    lines = tabix_res.split("\n")
    for line in lines:
        elements = line.split("\t")
        biomarker = elements[0] + ":" + elements[1] + elements[2] + ">" + elements[3]
        if biomarker in positions:
            res[biomarker]["score"] = elements[8]
            res[biomarker]["score_rounded"] = round_score(elements[8])
            res[biomarker]["uniprot_id"] = elements[5]
            res[biomarker]["transcript_id"] = elements[6]
            res[biomarker]["alphamissense_class"] = elements[9]
        else:
            if elements[0] + ":" + elements[1] in chrom_pos_positions:
                index = chrom_pos_positions.index(elements[0] + ":" + elements[1])
                position = positions[index]
                if "other_mutations_at_same_position" not in res[position]:
                    res[position]["other_mutations_at_same_position"] = {}
                aaexch = elements[2] + ">" + elements[3]
                res[position]["other_mutations_at_same_position"][aaexch] = {}
                res[position]["other_mutations_at_same_position"][aaexch]["score"] = elements[8]
                res[position]["other_mutations_at_same_position"][aaexch]["score_rounded"] = round_score(elements[8])
                res[position]["other_mutations_at_same_position"][aaexch]["uniprot_id"] = elements[5]
                res[position]["other_mutations_at_same_position"][aaexch]["transcript_id"] = elements[6]
                res[position]["other_mutations_at_same_position"][aaexch]["alphamissense_class"] = elements[9]
    return res


def get_score(genompos):
    res = {}
    positions = genompos.split(",")
    args = []
    args.append("tabix")
    args.append(conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")
    chrom_pos_positions = []

    for position in positions:
        try:
            chr, ref_seq, pos, ref, alt = parse_genome_position(position)
            tabix_pos = "chr" + chr + ":" + pos + "-" + pos
            args.append(tabix_pos)
            chrom_pos_positions.append("chr"+chr + ":" + pos)
        except:
            print(traceback.format_exc())
    #tabix_q = tabix_q.rstrip(",")

    print(args)
    res = subprocess.run(args,capture_output=True,text=True).stdout.rstrip("\n")
    print("Result: ",res)

    res = generate_result(chr,pos,ref,alt,positions,chrom_pos_positions,res)
    return res

def get_scores_for_region(chrom_list, start_pos_list, end_pos_list):
    req_res = {}
    start_positions = start_pos_list.split(",")
    end_positions = end_pos_list.split(",")
    chrom_positions = chrom_list.split(",")

    for i, start_pos in enumerate(start_positions):
        end_pos = end_positions[i]
        chrom = chrom_positions[i]

        args = []
        args.append("tabix")
        args.append(conf_reader.__MNT_PATH__ + "/AlphaMissense_hg38.tsv.gz")

        #chr, ref_seq, pos, ref, alt = parse_genome_position(start_pos)
        #chr_end, ref_seq_end, pos_end, ref_end, alt_end = parse_genome_position(end_pos)
        pos = "chr" + chrom + ":" + start_pos + "-" + end_pos
        args.append(pos)
        # tabix_q = tabix_q.rstrip(",")

        #print(args)
        res = subprocess.run(args, capture_output=True, text=True).stdout.rstrip("\n")
        #print("Result: ", res)
        qid = chrom+":"+start_pos+"-"+end_pos

        req_res = generate_result_region(qid, res,req_res)
    return req_res

