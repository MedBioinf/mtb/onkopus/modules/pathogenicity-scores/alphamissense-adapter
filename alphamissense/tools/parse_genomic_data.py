import re

def parse_genome_position(genompos):
    """
    Parses and returns the components of a genomic location. Returns the chromosome, reference sequence, postion, reference allele and alternate allele

    :param genompos:
    :return:
    """
    rsregex = "(NC_[0]+)([1-9|X|Y][0-9|X|Y]?).([0-9]+):(g.|c.)?([0-9]+)([A|C|G|T|-]+)>([A|C|G|T|-]+)"
    if re.compile(rsregex).match(genompos):
        p = re.compile(rsregex).match(genompos).groups()
        chr = p[1]
        pos = p[4]
        ref_seq = p[3]
        ref = p[5]
        alt = p[6]
    else:
        rsregex = "(CHR|chr)([0-9|X|Y|MT]+):(g.|c.)?([0-9]+)([A|C|G|T|-]+)>([A|C|G|T|-]+)"
        if re.compile(rsregex).match(genompos):
            p = re.compile(rsregex).match(genompos).groups()
            chr = p[1]
            pos = p[3]
            ref_seq = p[2]
            ref = p[4]
            alt = p[5]
        else:
            print("no match for genomic location: ",genompos)
    return chr, ref_seq, pos, ref, alt
